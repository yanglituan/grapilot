
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_socket_udp.h
  * @author     baiyang
  * @date       2021-10-1
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdbool.h>
#include <stdint.h>

#include <WinSock2.h>
#include <Ws2tcpip.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    TCHAR m_SrcIP[255];
    TCHAR m_DesIP[255];

    UINT  m_uDesSendPort;
    UINT  m_uSrcSendPort;
    UINT  m_uSrcRecvPort;

    SOCKET      m_sockStream;

    struct sockaddr_in m_SendAddr;
    struct sockaddr_in m_RecvAddr;

    bool connect;
} gp_socket_udp;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
// 构造函数
void socket_udp_ctor(gp_socket_udp *udp);

bool socket_udp_init(gp_socket_udp *udp, const char* lpDesIP, uint32_t uSrcRecvPort, uint32_t uDesRecvPort, const char* lpSrcIP, uint32_t uSrcSendPort);
int socket_udp_send(gp_socket_udp *udp, char* pBuf, int nSize);
int socket_udp_recv(gp_socket_udp *udp, char* pBuf, int nSize);
bool socket_udp_close(gp_socket_udp *udp);

static inline SOCKET socket_udp_get_socket(gp_socket_udp *udp) { return udp->m_sockStream; }
static inline bool socket_udp_is_connect(gp_socket_udp *udp) { return udp->connect; }
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



