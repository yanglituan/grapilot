@echo off
set AUTOTESTDIR="%~dp0\aircraft"

if exist "C:\PX4PSP\FlightGear 2016.1.2" (
    C:
    cd C:\PX4PSP\FlightGear 2016.1.2

    SET FG_ROOT=C:\PX4PSP\FlightGear 2016.1.2\data
    if not exist ".\\bin\fgfs.exe" exit
    start .\\bin\fgfs --aircraft=F450 --fg-aircraft=%AUTOTESTDIR% --fdm=null --native-fdm=socket,in,30,127.0.0.1,5503,udp --native-ctrls=socket,out,30,127.0.0.1,5505,udp --fog-fastest --disable-clouds --start-date-lat=2017:06:01:21:00:00 --disable-sound --in-air --disable-freeze --airport=KSF0 --runway=10L --altitude=0 --heading=0 --offset-distance=0 --offset-azimuth=0 --timeofday=noon
    exit
)

c:
FOR /F "delims=" %%D in ('dir /b "\Program Files"\FlightGear*') DO set FGDIR=%%D
::echo "Using FlightGear %FGDIR%"
cd "\Program Files\%FGDIR%\bin"

start fgfs ^
    --aircraft=F450 ^
    --fg-aircraft=%AUTOTESTDIR% ^
    --fdm=external ^
    --native-fdm=socket,in,30,,5503,udp ^
    --native-ctrls=socket,out,30,127.0.0.1,5505,udp ^
    --fdm=null ^
    --airport=KSFO ^
    --bpp=32 ^
    --disable-hud-3d ^
    --disable-horizon-effect ^
    --timeofday=noon ^
    --disable-sound ^
    --disable-fullscreen ^
    --disable-random-objects ^
    --disable-ai-models ^
    --fog-disable ^
    --disable-specular-highlight ^
    --disable-anti-alias-hud ^
    --wind=0@0

exit