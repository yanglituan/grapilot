
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       fms_state.c
  * @author     baiyang
  * @date       2021-8-24
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "fms.h"

#include <uITC/uITC.h>
#include <uITC/uITC_msg.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/
static uitc_actuator_armed actuator_armed;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void fms_set_auto_armed(bool b)
{
    // if no change, exit immediately
    if( fms.ap.auto_armed == b )
        return;

    fms.ap.auto_armed = b;
    if(b){
        //AP::logger().Write_Event(LogEvent::AUTO_ARMED);
    }
}

/**
  * @brief       
  * @param[in]   b  
  * @param[out]  
  * @retval      
  * @note        
  */
void fms_actuator_armed_notify(bool b)
{
    actuator_armed.armed = b;

    itc_publish(ITC_ID(vehicle_actuator_armed), &actuator_armed);
}

/*------------------------------------test------------------------------------*/


