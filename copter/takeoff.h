
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       takeoff.h
  * @author     baiyang
  * @date       2021-9-3
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdbool.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
// user-takeoff support; takeoff state is shared across all mode instances
typedef struct {
    bool _running;
    float take_off_start_alt;
    float take_off_complete_alt;
} UserTakeOff;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void takeoff_start(UserTakeOff* takeoff, float alt_cm);
static inline void takeoff_stop(UserTakeOff* takeoff) { takeoff->_running = false;}
void takeoff_do_pilot_takeoff(UserTakeOff* takeoff, float pilot_climb_rate);
bool takeoff_triggered(float target_climb_rate);
static inline bool takeoff_running(UserTakeOff* takeoff) { return takeoff->_running; }
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



