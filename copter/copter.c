
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       copter.c
  * @author     baiyang
  * @date       2021-7-12
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include <stdint.h>

#include <rtthread.h>

#include <board_config/borad_config.h>

#include <logger/task_logger.h>
#include <mavproxy/mavproxy.h>
#include <copter/fms.h>
#include <copter/mms.h>
#include <copter/ahrs.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
int copter_main(void)
{
    brd_init();

    task_comm_init();
    task_logger_init();
#ifdef GP_USING_EKF
    task_ahrs_ekf_init();
#endif
    task_fms_init();
    task_mms_init();
    return 1;
}

INIT_APP_EXPORT(copter_main);
/*------------------------------------test------------------------------------*/


