
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       fms_param.c
  * @author     baiyang
  * @date       2021-8-23
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "fms.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
void fms_param_assign()
{
    fms.flight_modes[0] = PARAM_GET_UINT8(FLIGHTMODE, FLTMODE1);
    fms.flight_modes[1] = PARAM_GET_UINT8(FLIGHTMODE, FLTMODE2);
    fms.flight_modes[2] = PARAM_GET_UINT8(FLIGHTMODE, FLTMODE3);
    fms.flight_modes[3] = PARAM_GET_UINT8(FLIGHTMODE, FLTMODE4);
    fms.flight_modes[4] = PARAM_GET_UINT8(FLIGHTMODE, FLTMODE5);
    fms.flight_modes[5] = PARAM_GET_UINT8(FLIGHTMODE, FLTMODE6);

    fms.g.failsafe_throttle = PARAM_GET_INT8(VEHICLE,FS_THR_ENABLE);
    fms.g.failsafe_throttle_value = PARAM_GET_INT16(VEHICLE,FS_THR_VALUE);
    fms.g.throttle_deadzone = PARAM_GET_INT16(VEHICLE,THR_DZ);
}

/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        1Hz
  */
void fms_param_update()
{
    fms_param_assign();

    // 姿态控制器参数
    attctrl_update_param(fms.attitude_control);

    // 位置控制器参数
    posctrl_update_param(fms.pos_control);

    // 
    RCs_UpdateParam(&(fms.channels));
}

/*------------------------------------test------------------------------------*/


