
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       task_serial.h
  * @author     baiyang
  * @date       2021-7-15
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>

#include <rthw.h>
#include <rtthread.h>
#include <rtdevice.h>

#include <serial_manager/gp_serial.h>
#include <serial_manager/gp_serial_device.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
typedef struct gp_serial_thread* gp_serial_thread_t;
struct gp_serial_thread_ops
{
    void (*init)(gp_serial_thread_t thread);
    void (*update)(void *parameter);
    void (*loop)(gp_serial_thread_t thread);
};


struct loop_buf
{
    uint8_t rec_flag;
    uint8_t rec_data;
    uint16_t send_len;
    uint8_t send_data[1024];
};
struct gp_serial_thread
{
    gp_serial_device_t sdevice;                   //设备
    const char *name;                             //设备名
    const char *event_name;                       //事件集名
    const char *timer_name;                       //定时器名
    rt_err_t res;                                 //返回值
    rt_uint32_t recv_set;                         //接受事件集
    rt_uint32_t wait_set;                         //触发事件
    struct rt_event event;                        //事件结构体
    struct rt_timer timer;                        //计数器结构体
    struct loop_buf buf;                          //循环用的缓冲区
    const struct gp_serial_thread_ops *ops;       //操作
    const char *sem_name;                         //信号量名
    rt_sem_t tx_sem;                              //用于DMA模式下触发回调的信号量
};

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
rt_err_t task_serial_init(void);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



