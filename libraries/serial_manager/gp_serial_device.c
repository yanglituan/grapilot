
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_serial_device..c
  * @author     baiyang
  * @date       2021-7-15
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "gp_serial_device.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       serial设备初始化
  * @param[in]   dev  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_err_t _init(rt_device_t dev)
{
    rt_size_t ret = 0;
    gp_serial_device_t serial;

    RT_ASSERT(dev != RT_NULL);

    serial = (gp_serial_device_t)dev;

    if(serial->ops->gp_serial_init)
    {
        ret = serial->ops->gp_serial_init(serial);
    }

    return ret;
}

/**
  * @brief       serial设备打开
  * @param[in]   dev  
  * @param[in]   oflag  无用参数，底层配置
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_err_t _open(rt_device_t dev, rt_uint16_t oflag)
{
    rt_size_t ret = 0;
    gp_serial_device_t serial;

    RT_ASSERT(dev != RT_NULL);

    serial = (gp_serial_device_t)dev;
    dev->open_flag = serial->oflag;

    if(serial->ops->gp_serial_open)
    {
        ret = serial->ops->gp_serial_open(serial);
    }

    return ret;
}

/**
  * @brief       serial设备关闭
  * @param[in]   dev  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_err_t _close(rt_device_t dev)
{
    rt_size_t ret = 0;
    gp_serial_device_t serial;

    RT_ASSERT(dev != RT_NULL);

    serial = (gp_serial_device_t)dev;

    if(serial->ops->gp_serial_close)
    {
        ret = serial->ops->gp_serial_close(serial);
    }

    return ret;
}

/**
  * @brief       serial设备读
  * @param[in]   dev  
  * @param[in]   pos  
  * @param[in]   buffer  
  * @param[in]   size  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_size_t _read(struct rt_device* dev, rt_off_t pos, void* buffer, rt_size_t size)
{
    rt_size_t ret = 0;
    gp_serial_device_t serial;

    RT_ASSERT(dev != RT_NULL);

    serial = (gp_serial_device_t)dev;

    if(serial->ops->gp_serial_read && buffer)
    {
        ret = serial->ops->gp_serial_read(serial, (uint16_t)pos, buffer, size);
    }

    return ret;
}
/**
  * @brief       serial设备写
  * @param[in]   dev  
  * @param[in]   pos  
  * @param[in]   buffer  
  * @param[in]   size  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_size_t _write(rt_device_t dev, rt_off_t pos, const void *buffer, rt_size_t size)
{
    rt_size_t ret = 0;
    gp_serial_device_t serial;

    RT_ASSERT(dev != RT_NULL);

    serial = (gp_serial_device_t)dev;

    if (serial->ops->gp_serial_write && buffer) {
        ret = serial->ops->gp_serial_write(serial, (uint16_t)pos, buffer, size);
    }

    return ret;
}

/**
  * @brief       serial设备控制
  * @param[in]   dev  
  * @param[in]   cmd  
  * @param[in]   args  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_err_t _control(struct rt_device* dev, int cmd, void* args)
{
    gp_serial_device_t serial;

    RT_ASSERT(dev != RT_NULL);

    serial = (gp_serial_device_t)dev;

    if(serial->ops->gp_serial_control) {
        return serial->ops->gp_serial_control(serial, cmd, args);
    }

    return RT_EOK;
}

/**
  * @brief       初始化时修改波特率等
  * @param[in]   serial  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_err_t gp_serial_init(gp_serial_device_t serial)
{
    rt_err_t ret;

    struct serial_configure conf = RT_SERIAL_CONFIG_DEFAULT;

    // 根据参数SERIALx_BAUD设置串口波特率
    conf.baud_rate = serial->baud;
    ret = rt_device_control(serial->puart, RT_DEVICE_CTRL_CONFIG, &conf); //修改参数
    return ret;
}

/**
  * @brief       打开对应串口
  * @param[in]   serial  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_err_t gp_serial_open(gp_serial_device_t serial)
{

    return rt_device_open(serial->puart, serial->oflag);

}

/**
  * @brief       关闭对应串口
  * @param[in]   serial  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_err_t gp_serial_close(gp_serial_device_t serial)
{

    return rt_device_close(serial->puart);

}

/**
  * @brief       直接从对应串口读数据
  * @param[in]   serial  
  * @param[in]   pos  
  * @param[in]   buffer  
  * @param[in]   size  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_size_t gp_serial_read(gp_serial_device_t serial, rt_off_t pos, void* buffer, rt_size_t size)
{
    rt_size_t ret;
    ret = rt_ringbuffer_get(serial->rb_rx, buffer, size);
    return ret;
}

/**
  * @brief       向发送FIFO中存入待发数据
  * @param[in]   serial  
  * @param[in]   pos  
  * @param[in]   buffer  
  * @param[in]   size  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_size_t gp_serial_write(gp_serial_device_t serial, rt_off_t pos, const void* buffer, rt_size_t size)
{
    rt_size_t ret;
    ret = rt_ringbuffer_put_force(serial->rb_tx, buffer, size); //满时，新数据覆盖旧数据
    return ret;
}

/**
  * @brief       控制串口配置参数
  * @param[in]   serial  
  * @param[in]   cmd  
  * @param[in]   arg  
  * @param[out]  
  * @retval      
  * @note        
  */
static rt_err_t gp_serial_control(gp_serial_device_t serial, int cmd, void* arg)
{
    rt_err_t ret;
    ret = rt_device_control(serial->puart, cmd, arg);
    return ret;
}

const struct gp_serial_ops _serial_ops = {
    .gp_serial_init = gp_serial_init,
    .gp_serial_open = gp_serial_open,
    .gp_serial_close = gp_serial_close,
    .gp_serial_read = gp_serial_read,
    .gp_serial_write = gp_serial_write,
    .gp_serial_control = gp_serial_control
};


/**
  * @brief       将各个设备注册
  * @param[in]   serial  
  * @param[in]   name  
  * @param[in]   flag  
  * @param[out]  
  * @retval      
  * @note        
  */
rt_err_t gp_serial_device_register(gp_serial_device_t serial, const char* name, rt_uint32_t flag)
{
    struct rt_device* device;

    RT_ASSERT(serial != RT_NULL);

    serial->ops = &_serial_ops;

    device = &(serial->parent);
    device->type        = RT_Device_Class_Char; //是否修改为这个？
    device->ref_count   = 0;
    device->rx_indicate = RT_NULL;
    device->tx_complete = RT_NULL;

    device->init        = _init;
    device->open        = _open;
    device->close       = _close;
    device->read        = _read;
    device->write       = _write;
    device->control     = _control;

    return rt_device_register(device, name, flag);
}

/*------------------------------------test------------------------------------*/


