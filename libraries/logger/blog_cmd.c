
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       blog_cmd.c
  * @author     baiyang
  * @date       2021-8-7
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include <string.h>
#include <rtthread.h>

#include "blog.h"
#include "task_logger.h"

#include <common/console/console.h>
/*-----------------------------------macro------------------------------------*/
#define STRING_COMPARE(str1, str2)              (strcmp(str1, str2) == 0)

/*----------------------------------typedef-----------------------------------*/
typedef struct {
    char* opt;
    char* val;
} optv_t;
/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
static void _show_blog_status(void)
{
    if (blog_get_status() == BLOG_STATUS_IDLE) {
        console_printf("no working log file.\n");
        return;
    }

    console_printf("log file: %s\n", blog_get_logging_file_name());
    blog_show_status();
}

static void show_usage(void)
{
}

static int blog_cmd(int argc, char** argv)
{
    if (argc < 2) {
        show_usage();
        return 0;
    }

    if (strcmp(argv[1], "start") == 0) {
        if (argc >= 3) {
            logger_start_blog(argv[2]);
        } else {
            logger_start_blog(NULL);
        }
    } else if (strcmp(argv[1], "stop") == 0) {
        logger_stop_blog();
    } else if (strcmp(argv[1], "status") == 0) {
        _show_blog_status();
    }else if (strcmp(argv[1], "ws") == 0) {
        char path[100];
        get_working_log_session(path);
        console_printf("working log session: %s\n", path);
    } else {
        show_usage();
    }

    return 0;
}

MSH_CMD_EXPORT(blog_cmd,blog cmd);
/*------------------------------------test------------------------------------*/


