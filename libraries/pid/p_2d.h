
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       p_2d.h
  * @author     baiyang
  * @date       2021-8-29
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdbool.h>
#include <common/gp_defines.h>
#include <common/gp_math/gp_mathlib.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    // parameters
    float      _kp;

    // internal variables
    float      _dt;          // time step in seconds
    Vector2f_t _error;       // time step in seconds
    float      _error_max;   // error limit in positive direction
    float      _D1_max;      // maximum first derivative of output
} P_2d_ctrl;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
// constructor
void p_2d_ctrl_ctor(P_2d_ctrl* controler, float initial_p, float dt);

// set time step in seconds
static inline void p_2d_ctrl_set_dt(P_2d_ctrl* controler, float dt) { controler->_dt = dt; }

// set target and measured inputs to P controller and calculate outputs
Vector2f_t p_2d_ctrl_update_all(P_2d_ctrl* controler, postype_t *target_x, postype_t *target_y, const Vector2f_t *measurement, bool *limit);

// set target and measured inputs to P controller and calculate outputs
// measurement is provided as 3-axis vector but only x and y are used
static inline Vector2f_t p_2d_ctrl_update_all2(P_2d_ctrl* controler, postype_t *target_x, postype_t *target_y, const Vector3f_t *measurement, bool *limit) {
    Vector2f_t _measurement = {measurement->x, measurement->y};
    return p_2d_ctrl_update_all(controler, target_x, target_y, &_measurement, limit);
}

// set_limits - sets the maximum error to limit output and first and second derivative of output
void p_2d_ctrl_set_limits(P_2d_ctrl* controler, float output_max, float D_Out_max, float D2_Out_max);

// set_error_max - reduce maximum position error to error_max
// to be called after setting limits
void p_2d_ctrl_set_error_max(P_2d_ctrl* controler, float error_max);

// get_error_max - return maximum position error
static inline float p_2d_ctrl_get_error_max(P_2d_ctrl* controler) { return controler->_error_max; }

// save gain to eeprom

// get accessors
static inline float p_2d_ctrl_get_kP(P_2d_ctrl* controler) { return controler->_kp; }
static inline Vector2f_t p_2d_ctrl_get_error(P_2d_ctrl* controler) { return controler->_error; }

// set accessors
static inline void p_2d_ctrl_set_kP(P_2d_ctrl* controler, float v) { controler->_kp = v; }

// proportional controller with piecewise sqrt sections to constrain second derivative
Vector2f_t p_2d_ctrl_sqrt_controller(const Vector2f_t* error, float p, float second_ord_lim, float dt);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



