
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uITC_msg.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include "./msg/uitc_mode_loiter.h"
#include "./msg/uitc_sensor_acc.h"
#include "./msg/uitc_sensor_baro.h"
#include "./msg/uitc_sensor_gps.h"
#include "./msg/uitc_sensor_gyr.h"
#include "./msg/uitc_sensor_mag.h"
#include "./msg/uitc_vehicle_actuator.h"
#include "./msg/uitc_vehicle_alt.h"
#include "./msg/uitc_vehicle_att_ctrl.h"
#include "./msg/uitc_vehicle_attitude.h"
#include "./msg/uitc_vehicle_battery.h"
#include "./msg/uitc_vehicle_hil.h"
#include "./msg/uitc_vehicle_home.h"
#include "./msg/uitc_vehicle_pos_ctrl.h"
#include "./msg/uitc_vehicle_rc.h"
#include "./msg/uitc_digital_airplane.h"
#include "./msg/uitc_vehicle_position.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
// 初始化uITC模块包含的消息
bool uitc_msg_init();
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



