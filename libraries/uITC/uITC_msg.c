
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uITC_msg.c
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include "uITC.h"
#include "uITC_msg.h"

#include <common/console/console.h>
/*-----------------------------------macro------------------------------------*/
ITC_DEFINE(mode_loiter, sizeof(uitc_mode_loiter));
ITC_DEFINE(sensor_acc, sizeof(uitc_sensor_acc));
ITC_DEFINE(sensor_baro, sizeof(uitc_sensor_baro));
ITC_DEFINE(sensor_baro_raw, sizeof(uitc_sensor_baro_raw));

ITC_DEFINE(sensor_gps, sizeof(uitc_sensor_gps));
ITC_DEFINE(sensor_gyr, sizeof(uitc_sensor_gyr));
ITC_DEFINE(sensor_mag, sizeof(uitc_sensor_mag));

ITC_DEFINE(vehicle_actuator_controls, sizeof(uitc_actuator_controls));
ITC_DEFINE(vehicle_actuator_outputs, sizeof(uitc_actuator_outputs));
ITC_DEFINE(vehicle_actuator_armed, sizeof(uitc_actuator_armed));

ITC_DEFINE(vehicle_alt, sizeof(uitc_vehicle_alt));
ITC_DEFINE(vehicle_attitude, sizeof(uitc_vehicle_attitude));

ITC_DEFINE(vehicle_att_ctrl, sizeof(uitc_vehicle_att_ctrl));
ITC_DEFINE(vehicle_battery, sizeof(uitc_vehicle_battery));
ITC_DEFINE(vehicle_hil_state, sizeof(uitc_vehicle_hil_state));
ITC_DEFINE(vehicle_home, sizeof(uitc_vehicle_home));
ITC_DEFINE(vehicle_origin, sizeof(uitc_vehicle_origin));
ITC_DEFINE(vehicle_pos_ctrl_xy, sizeof(uitc_vehicle_pos_ctrl_xy));
ITC_DEFINE(vehicle_pos_ctrl_z, sizeof(uitc_vehicle_pos_ctrl_z));
ITC_DEFINE(vehicle_rc, sizeof(uitc_actuator_rc));
ITC_DEFINE(digital_airplane_state, sizeof(uitc_digital_airplane_state));
ITC_DEFINE(vehicle_position, sizeof(uitc_vehicle_position));
/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
/**
  * @brief       
  * @param[in]     
  * @param[out]  
  * @retval      
  * @note        
  */
bool uitc_msg_init()
{
    bool res = false;

    int itc_res;
    itc_res = itc_advertise(ITC_ID(mode_loiter));
    if(itc_res != 0){
        console_printf("uitc err:%d, mode_loiter advertise fail!\n", itc_res);
    }

    itc_res = itc_advertise(ITC_ID(sensor_acc));
    if(itc_res != 0){
        console_printf("uitc err:%d, sensor_acc advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(sensor_baro));
    if(itc_res != 0){
        console_printf("uitc err:%d, sensor_baro advertise fail!\n", itc_res);
    }

    itc_res = itc_advertise(ITC_ID(sensor_baro_raw));
    if(itc_res != 0){
        console_printf("uitc err:%d, sensor_baro_raw advertise fail!\n", itc_res);
    }

    itc_res = itc_advertise(ITC_ID(sensor_gps));
    if(itc_res != 0){
        console_printf("uitc err:%d, sensor_gps advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(sensor_gyr));
    if(itc_res != 0){
        console_printf("uitc err:%d, sensor_gyr advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(sensor_mag));
    if(itc_res != 0){
        console_printf("uitc err:%d, sensor_mag advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_actuator_controls));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_actuator_controls advertise fail!\n", itc_res);
    }

    itc_res = itc_advertise(ITC_ID(vehicle_actuator_outputs));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_actuator_outputs advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_actuator_armed));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_actuator_armed advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_alt));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_alt advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_attitude));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_attitude advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_att_ctrl));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_att_ctrl advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_battery));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_battery advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_hil_state));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_hil_state advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_home));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_home advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_origin));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_origin advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_pos_ctrl_xy));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_pos_ctrl_xy advertise fail!\n", itc_res);
    }

    itc_res = itc_advertise(ITC_ID(vehicle_pos_ctrl_z));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_pos_ctrl_z advertise fail!\n", itc_res);
    }
    
    itc_res = itc_advertise(ITC_ID(vehicle_rc));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_rc advertise fail!\n", itc_res);
    }

    itc_res = itc_advertise(ITC_ID(digital_airplane_state));
    if(itc_res != 0){
        console_printf("uitc err:%d, digital_airplane_state advertise fail!\n", itc_res);
    }

    itc_res = itc_advertise(ITC_ID(vehicle_position));
    if(itc_res != 0){
        console_printf("uitc err:%d, vehicle_position advertise fail!\n", itc_res);
    }
    
    if (itc_res == 0) {
        res = true;
    }

    return res;
}

/*------------------------------------test------------------------------------*/


