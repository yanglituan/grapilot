
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_vehicle_battery.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(vehicle_battery);
/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    uint64_t    timestamp_us;              // 最后一次读取电压和电流的时间（以微秒为单位）
    float       board_voltage;             // 板子供电电压（伏）
    float       batt_voltage;              // 电池电压（伏）
    float       batt_current_amps;         // 电池电流（安培）
    float       consumed_mah;              // 自启动以来的总电流（以毫安小时为单位）
    float       consumed_wh;               // 自启动以来的消耗的能量（以瓦时为单位）
} uitc_vehicle_battery;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



