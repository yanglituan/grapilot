
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_vehicle_alt.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(vehicle_alt);
/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    uint64_t timestamp_us;

    float alt;             /* AMSL, m, up direction */
    float relative_alt;    /* relative altitude, m, up direction */
    float vz;              /* velocity of z direction, m/s, Navigation coordinate system,up direction */
    float az;              /* acceleration of z direction, Navigation coordinate system, m/s/s, up direction,does not include gravity promotions */
    float az_bias;         /* bias of z axis acceleration, Navigation coordinate system, up direction */
} uitc_vehicle_alt;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



