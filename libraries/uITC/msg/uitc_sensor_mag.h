
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_sensor_mag.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(sensor_mag);
/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    uint64_t timestamp_us;

    float sensor_mag_measure[3];    //磁罗盘原始数，坐标NED
    float sensor_mag_correct[3];    //磁罗盘校准后的数据，坐标NED
    float sensor_mag_filter[3];     //磁罗盘校准+滤波后的数据，坐标NED
} uitc_sensor_mag;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



