
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_sensor_baro.h
  * @author     baiyang
  * @date       2021-7-13
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>
/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(sensor_baro);
ITC_DECLARE(sensor_baro_raw);

/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    uint64_t timestamp_us;

    float altitude_cm;    // 坐标轴向上
    float velocity_cms;   // 坐标轴向上
} uitc_sensor_baro;

typedef struct {
    uint64_t timestamp_us;

    uint32_t raw_temperature;
    uint32_t raw_pressure;
    
    float temperature_deg;
    float pressure_Pa;
    float altitude_m;
} uitc_sensor_baro_raw;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



