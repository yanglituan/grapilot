
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       uitc_vehicle_position.h
  * @author     baiyang
  * @date       2021-7-27
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <uITC/uITC.h>

/*-----------------------------------macro------------------------------------*/
ITC_DECLARE(vehicle_position);
/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    uint64_t timestamp_us;

    int32_t lat; /**< Latitude in 1E-7 degrees, ekf output*/
    int32_t lon; /**< Longitude in 1E-7 degrees, ekf output*/

    float x;  // in meters NED
    float y;  // in meters NED
    
    float vx; // in meters/sec
    float vy; // in meters/sec

    float ax; // in meters/sec^2
    float ay; // in meters/sec^2

    float ax_bias; // in meters/sec^2
    float ay_bias; // in meters/sec^2
} uitc_vehicle_position;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



