
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       storage_manager.h
  * @author     baiyang
  * @date       2021-8-8
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdbool.h>
#include <stdint.h>
/*-----------------------------------macro------------------------------------*/
#define STORAGE_MISSION_LENGTH 1024*3
/*----------------------------------typedef-----------------------------------*/
typedef enum {
    StorageMission = 3,
} StorageType;

typedef struct {
    StorageType type;
    uint16_t    offset;
    uint16_t    length;
} StorageArea;

typedef struct
{
    StorageType type;
    uint16_t total_size;
} StorageAccess_HandleTypeDef;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
//
void StorageAccess(StorageAccess_HandleTypeDef *StorageAccess, StorageType type);

// return total size of this accessor
uint16_t StorageAccess_size(StorageAccess_HandleTypeDef *StorageAccess);

// base access via block functions
bool StorageAccess_ReadBlock(StorageAccess_HandleTypeDef *StorageAccess, void *data, uint16_t addr, uint32_t n);
bool StorageAccess_WriteBlock(StorageAccess_HandleTypeDef *StorageAccess, uint16_t addr, const void *data, uint32_t n);

/*
  read a byte
 */
uint8_t StorageAccess_ReadByte(StorageAccess_HandleTypeDef *StorageAccess, uint16_t loc);

/*
  read 16 bit value
 */
uint16_t StorageAccess_ReadUint16(StorageAccess_HandleTypeDef *StorageAccess, uint16_t loc);

/*
  read 32 bit value
 */
uint32_t StorageAccess_ReadUint32(StorageAccess_HandleTypeDef *StorageAccess, uint16_t loc);

/*
  write a byte
 */
void StorageAccess_WriteByte(StorageAccess_HandleTypeDef *StorageAccess, uint16_t loc, uint8_t value);

/*
  write a uint16
 */
void StorageAccess_WriteUint16(StorageAccess_HandleTypeDef *StorageAccess, uint16_t loc, uint16_t value);

/*
  write a uint32
 */
void StorageAccess_WriteUint32(StorageAccess_HandleTypeDef *StorageAccess, uint16_t loc, uint32_t value);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



