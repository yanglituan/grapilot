
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_math.h
  * @author     baiyang
  * @date       2021-7-5
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include "math_def.h"
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
float math_asinf(const float val);

float math_sqrtf(float val);
float math_rsqrtf(float number);

float math_constrain_float(float amt, float low, float high);
double math_constrain_double(double amt, double low, double high);

uint32_t math_constrain_uint32(uint32_t amt, uint32_t low, uint32_t high);
int32_t math_constrain_int32(int32_t amt, int32_t low, int32_t high);

uint16_t math_constrain_uint16(uint16_t amt, uint16_t low, uint16_t high);
int16_t math_constrain_int16(int16_t amt, int16_t low, int16_t high);

double math_rand_normal(double mean, double stddev);

float math_norm(const float first, const float second);

bool math_flt_zero(float val);
bool math_flt_equal(float val1, float val2);
bool math_flt_negative(float val);
bool math_flt_positive(float val);

bool math_dbl_zero(double val);
bool math_dbl_negative(double val);
bool math_dbl_positive(double val);

float math_linear_inter(float low_output, float high_output,
                         float var_value,
                         float var_low, float var_high);

float math_wrap_PI(const float radian);
float math_wrap_2PI(const float radian);

float math_wrap_360(const float angle);
float math_wrap_360_cd(float angle);

float math_wrap_180(float angle);
float math_wrap_180_cd(float angle);

float math_calc_lpf_alpha_dt(float dt, float cutoff_freq);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



