
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_time.h
  * @author     baiyang
  * @date       2021-7-8
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include "rtconfig.h"
/*-----------------------------------macro------------------------------------*/
#if CONFIG_HAL_BOARD == HAL_BOARD_SITL_WIN
#define DEFINE_TIMETAG(_name, _period)      \
    static TimeTag __timetag_##_name = {    \
        .0,                           \
        _period                   \
    }
#else
#define DEFINE_TIMETAG(_name, _period)      \
    static TimeTag __timetag_##_name = {    \
        .tag = 0,                           \
        .period = _period                   \
    }
#endif

#define TIMETAG(_name)          (&__timetag_##_name)

#define TIMETAG_CHECK_EXECUTE(_name, _period, _operation)   \
    DEFINE_TIMETAG(_name, _period); \
    if(time_check_tag(TIMETAG(_name))){_operation}

#define TIMETAG_CHECK_EXECUTE2(_name, _period, _time_now, _operation)   \
    DEFINE_TIMETAG(_name, _period); \
    if(time_check_tag2(TIMETAG(_name), _time_now)){_operation}

#define TIMETAG_CHECK_EXECUTE3(_name, _period, _time_now, _operation)   \
    DEFINE_TIMETAG(_name, 0); \
    if(time_check_tag3(TIMETAG(_name), _time_now, _period)){_operation}
/*----------------------------------typedef-----------------------------------*/
typedef struct 
{
    uint32_t ticksPerUs;              //每us的tick数 168M/1e6=168
    uint32_t ticksPerMs;              //每ms的tick数 168M/1e3=168000
}Time_def;

typedef struct {
    uint32_t tag;
    uint32_t period;
} TimeTag;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void     time_init(void);       //系统初始化调用

uint32_t time_micros(void);
uint64_t time_micros64(void);

uint32_t time_millis(void);
uint16_t time_millis16(void);
uint64_t time_millis64(void);

uint8_t time_check_tag(TimeTag* timetag);
uint8_t time_check_tag2(TimeTag* timetag, uint32_t now);
uint8_t time_check_tag3(TimeTag* timetag, uint32_t now, uint32_t period);

void time_delay_us(uint32_t time_us);
void time_delay_ms(uint32_t time_ms);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



