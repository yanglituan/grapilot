
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       gp_defines.h
  * @author     baiyang
  * @date       2021-7-9
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <common/gp_math/gp_vector2.h>
#include <common/gp_math/gp_vector3.h>
/*-----------------------------------macro------------------------------------*/
typedef float ftype;
typedef float postype_t;
typedef Vector2f_t Vector2p;
typedef Vector3f_t Vector3p;

#ifndef ARRAY_SIZE
# define ARRAY_SIZE(ar) (sizeof(ar) / sizeof(ar[0]))
#endif

#ifdef __GNUC__
#define GP_PACKED(__Declaration__) __Declaration__ __attribute__((packed))
#else
#define GP_PACKED(__Declaration__) __pragma(pack(push, 1)) __Declaration__ __pragma(pack(pop))
#endif

// Yaw behaviours during missions - possible values for WP_YAW_BEHAVIOR parameter
#define WP_YAW_BEHAVIOR_NONE                          0   // auto pilot will never control yaw during missions or rtl (except for DO_CONDITIONAL_YAW command received)
#define WP_YAW_BEHAVIOR_LOOK_AT_NEXT_WP               1   // auto pilot will face next waypoint or home during rtl
#define WP_YAW_BEHAVIOR_LOOK_AT_NEXT_WP_EXCEPT_RTL    2   // auto pilot will face next waypoint except when doing RTL at which time it will stay in it's last
#define WP_YAW_BEHAVIOR_LOOK_AHEAD                    3   // auto pilot will look ahead during missions and rtl (primarily meant for traditional helicotpers)

// Radio failsafe definitions (FS_THR parameter)
#define FS_THR_DISABLED                            0
#define FS_THR_ENABLED_ALWAYS_RTL                  1
#define FS_THR_ENABLED_CONTINUE_MISSION            2    // Removed in 4.0+, now use fs_options
#define FS_THR_ENABLED_ALWAYS_LAND                 3
#define FS_THR_ENABLED_ALWAYS_SMARTRTL_OR_RTL      4
#define FS_THR_ENABLED_ALWAYS_SMARTRTL_OR_LAND     5
#define FS_THR_ENABLED_AUTO_RTL_OR_RTL             6

// GCS failsafe definitions (FS_GCS_ENABLE parameter)
#define FS_GCS_DISABLED                        0
#define FS_GCS_ENABLED_ALWAYS_RTL              1
#define FS_GCS_ENABLED_CONTINUE_MISSION        2    // Removed in 4.0+, now use fs_options
#define FS_GCS_ENABLED_ALWAYS_SMARTRTL_OR_RTL  3
#define FS_GCS_ENABLED_ALWAYS_SMARTRTL_OR_LAND 4
#define FS_GCS_ENABLED_ALWAYS_LAND             5
#define FS_GCS_ENABLED_AUTO_RTL_OR_RTL         6

// EKF failsafe definitions (FS_EKF_ACTION parameter)
#define FS_EKF_ACTION_LAND                  1       // switch to LAND mode on EKF failsafe
#define FS_EKF_ACTION_ALTHOLD               2       // switch to ALTHOLD mode on EKF failsafe
#define FS_EKF_ACTION_LAND_EVEN_STABILIZE   3       // switch to Land mode on EKF failsafe even if in a manual flight mode like stabilize

// for PILOT_THR_BHV parameter
#define THR_BEHAVE_FEEDBACK_FROM_MID_STICK (1<<0)
#define THR_BEHAVE_HIGH_THROTTLE_CANCELS_LAND (1<<1)
#define THR_BEHAVE_DISARM_ON_LAND_DETECT (1<<2)
/*----------------------------------typedef-----------------------------------*/
typedef Vector3f_t Param_Vector3f;
typedef float      Param_float;
typedef int32_t    Param_int32;
typedef uint32_t   Param_uint32;
typedef int16_t    Param_int16;
typedef uint16_t   Param_uint16;
typedef int8_t     Param_int8;
typedef uint8_t    Param_uint8;

// Autopilot Yaw Mode enumeration
typedef enum {
    AUTO_YAW_HOLD =             0,  // pilot controls the heading
    AUTO_YAW_LOOK_AT_NEXT_WP =  1,  // point towards next waypoint (no pilot input accepted)
    AUTO_YAW_ROI =              2,  // point towards a location held in roi_WP (no pilot input accepted)
    AUTO_YAW_LOOK_AT_HEADING =  3,  // point towards a particular angle (not pilot input accepted)
    AUTO_YAW_LOOK_AHEAD =       4,  // point in the direction the copter is moving
    AUTO_YAW_RESETTOARMEDYAW =  5,  // point towards heading at time motors were armed
    AUTO_YAW_RATE =             6,  // turn at a specified rate (held in auto_yaw_rate)
    AUTO_YAW_CIRCLE =           7,  // use AC_Circle's provided yaw (used during Loiter-Turns commands)
} Auto_yaw_mode;

typedef enum {
    GP_EOK = 0,         /**< 没有错误 */
    GP_ERROR = 1,       /**< 普通错误 */
    GP_ETIMEOUT = 2,    /**< 超时 */
    GP_EFULL = 3,       /**< 资源已满 */
    GP_EEMPTY = 4,      /**< 资源为空 */
    GP_ENOMEM = 5,      /**< 没有存储空间 */
    GP_ENOSYS = 6,      /**< 没有系统*/
    GP_EBUSY = 7,       /**< 忙 */
    GP_EIO = 8,         /**< 输入输出错误 */
    GP_EINTR = 9,       /**< 中断系统调用 */
    GP_EINVAL = 10,     /**< 无效参数 */
    GP_ENOTHANDLE = 11, /**< 未处理 */
} gp_err;

// Auto modes
typedef enum {
    Auto_TakeOff,
    Auto_WP,
    Auto_Land,
    Auto_RTL,
    Auto_CircleMoveToEdge,
    Auto_Circle,
    Auto_Spline,
    Auto_NavGuided,
    Auto_Loiter,
    Auto_LoiterToAlt,
    Auto_NavPayloadPlace,
} AutoMode;

// interface to set the vehicles mode
typedef enum {
    MODE_REASON_UNKNOWN = 0,
    MODE_REASON_RC_COMMAND = 1,
    MODE_REASON_GCS_COMMAND = 2,
    MODE_REASON_RADIO_FAILSAFE = 3,
    MODE_REASON_BATTERY_FAILSAFE = 4,
    MODE_REASON_GCS_FAILSAFE = 5,
    MODE_REASON_EKF_FAILSAFE = 6,
    MODE_REASON_GPS_GLITCH = 7,
    MODE_REASON_MISSION_END = 8,
    MODE_REASON_THROTTLE_LAND_ESCAPE = 9,
    MODE_REASON_FENCE_BREACHED = 10,
    MODE_REASON_TERRAIN_FAILSAFE = 11,
    MODE_REASON_BRAKE_TIMEOUT = 12,
    MODE_REASON_FLIP_COMPLETE = 13,
    MODE_REASON_AVOIDANCE = 14,
    MODE_REASON_AVOIDANCE_RECOVERY = 15,
    MODE_REASON_THROW_COMPLETE = 16,
    MODE_REASON_TERMINATE = 17,
    MODE_REASON_TOY_MODE = 18,
    MODE_REASON_CRASH_FAILSAFE = 19,
    MODE_REASON_SOARING_FBW_B_WITH_MOTOR_RUNNING = 20,
    MODE_REASON_SOARING_THERMAL_DETECTED = 21,
    MODE_REASON_SOARING_THERMAL_ESTIMATE_DETERIORATED = 22,
    MODE_REASON_VTOL_FAILED_TRANSITION = 23,
    MODE_REASON_VTOL_FAILED_TAKEOFF = 24,
    MODE_REASON_FAILSAFE = 25, // general failsafes, prefer specific failsafes over this as much as possible
    MODE_REASON_INITIALISED = 26,
    MODE_REASON_SURFACE_COMPLETE = 27,
    MODE_REASON_BAD_DEPTH = 28,
    MODE_REASON_LEAK_FAILSAFE = 29,
    MODE_REASON_SERVOTEST = 30,
    MODE_REASON_STARTUP = 31,
    MODE_REASON_SCRIPTING = 32,
    MODE_REASON_UNAVAILABLE = 33,
    MODE_REASON_AUTOROTATION_START = 34,
    MODE_REASON_AUTOROTATION_BAILOUT = 35,
    MODE_REASON_SOARING_ALT_TOO_HIGH = 36,
    MODE_REASON_SOARING_ALT_TOO_LOW = 37,
    MODE_REASON_SOARING_DRIFT_EXCEEDED = 38,
    MODE_REASON_RTL_COMPLETE_SWITCHING_TO_VTOL_LAND_RTL = 39,
    MODE_REASON_RTL_COMPLETE_SWITCHING_TO_FIXEDWING_AUTOLAND = 40,
    MODE_REASON_MISSION_CMD = 41,
    MODE_REASON_FRSKY_COMMAND = 42,
    MODE_REASON_FENCE_RETURN_PREVIOUS_MODE = 43,
    MODE_REASON_QRTL_INSTEAD_OF_RTL = 44,
} ModeReason;

// Airmode
typedef enum  {
    AIRMODE_NONE,
    AIRMODE_DISABLED,
    AIRMODE_ENABLED,
} AirMode;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



