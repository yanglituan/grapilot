
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       mavcmd.c
  * @author     baiyang
  * @date       2021-7-21
  ******************************************************************************
  */

/*----------------------------------include-----------------------------------*/
#include <string.h>

#include "mavcmd.h"
#include <ftp/ftp_manager.h>
/*-----------------------------------macro------------------------------------*/
#define TAG "MAVCMD"
/*----------------------------------typedef-----------------------------------*/

/*---------------------------------prototype----------------------------------*/

/*----------------------------------variable----------------------------------*/
static uint8_t _mavcmd_set[MAVCMD_ITEM_NUM] = { 0 };
static StreamSession* _ftp_stream_session = NULL;
/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void mavcmd_set(MavCmd_ID cmd, void* data)
{
    if (cmd == MAVCMD_CALIBRATION_GYR) {

    } else if (cmd == MAVCMD_CALIBRATION_ACC) {

    } else if (cmd == MAVCMD_CALIBRATION_MAG) {

    } else if (cmd == MAVCMD_STREAM_SESSION) {

        if (data) {
            /* ftp stream session */
            _ftp_stream_session = (StreamSession*)data;
            _mavcmd_set[cmd] = 1;
        }
    } else {
        /* unknown command */
        return;
    }
}

void mavcmd_clear(MavCmd_ID cmd)
{
    if (cmd < MAVCMD_ITEM_NUM) {
        _mavcmd_set[cmd] = 0;
    }
}

void mavcmd_process(void)
{
    if (_mavcmd_set[MAVCMD_STREAM_SESSION]) {
        ftp_stream_send(_ftp_stream_session);
    }
}
/*------------------------------------test------------------------------------*/


