
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       mavlink_param.h
  * @author     baiyang
  * @date       2021-7-22
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <parameter/param.h>
#include <common/gp_rotations.h>
#include <common/gp_defines.h>
/*-----------------------------------macro------------------------------------*/
#define MAVLINK_PARAM_DECLARE(_name)        mav_param_t _name

#define MAVLINK_PARAM_DEFINE(_name, _value) \
        { \
            .name = #_name, \
            .value = _value, \
            .param = NULL \
        }

#define MAVLINK_PARAM_DEFINE_FULL(_name, _value, _param) \
        { \
            .name = #_name, \
            .value = _value, \
            .param = _param \
        }

/*----------------------------------typedef-----------------------------------*/
typedef struct {
    const char* name;
    float value;
    param_t* param;
} mav_param_t;

typedef struct 
{
    MAVLINK_PARAM_DECLARE(FORMAT_VERSION);
    MAVLINK_PARAM_DECLARE(SYSID_THISMAV);
    MAVLINK_PARAM_DECLARE(SYSID_MYGCS);

    MAVLINK_PARAM_DECLARE(COMPASS_USE);
    MAVLINK_PARAM_DECLARE(COMPASS_USE2);
    MAVLINK_PARAM_DECLARE(COMPASS_DEV_ID);
    MAVLINK_PARAM_DECLARE(COMPASS_DEV_ID2);
    MAVLINK_PARAM_DECLARE(COMPASS_DEV_ID3);
    MAVLINK_PARAM_DECLARE(COMPASS_PRIMARY);
    MAVLINK_PARAM_DECLARE(COMPASS_OFS_X);
    MAVLINK_PARAM_DECLARE(COMPASS_OFS_Y);
    MAVLINK_PARAM_DECLARE(COMPASS_OFS_Z);
    MAVLINK_PARAM_DECLARE(COMPASS_OFS2_X);
    MAVLINK_PARAM_DECLARE(COMPASS_OFS2_Y);
    MAVLINK_PARAM_DECLARE(COMPASS_OFS2_Z);
    MAVLINK_PARAM_DECLARE(COMPASS_OFS3_X);
    MAVLINK_PARAM_DECLARE(COMPASS_OFS3_Y);
    MAVLINK_PARAM_DECLARE(COMPASS_OFS3_Z);

    MAVLINK_PARAM_DECLARE(BATT_MONITOR);
    MAVLINK_PARAM_DECLARE(ARMING_CHECK);

    MAVLINK_PARAM_DECLARE(INS_ACCOFFS_X);
    MAVLINK_PARAM_DECLARE(INS_ACCOFFS_Y);
    MAVLINK_PARAM_DECLARE(INS_ACCOFFS_Z);
    MAVLINK_PARAM_DECLARE(INS_ACCSCAL_X);
    MAVLINK_PARAM_DECLARE(INS_ACCSCAL_Y);
    MAVLINK_PARAM_DECLARE(INS_ACCSCAL_Z);

    MAVLINK_PARAM_DECLARE(FLTMODE1);
    MAVLINK_PARAM_DECLARE(FLTMODE2);
    MAVLINK_PARAM_DECLARE(FLTMODE3);
    MAVLINK_PARAM_DECLARE(FLTMODE4);
    MAVLINK_PARAM_DECLARE(FLTMODE5);
    MAVLINK_PARAM_DECLARE(FLTMODE6);

    MAVLINK_PARAM_DECLARE(MNT_RC_IN_ROLL);
    MAVLINK_PARAM_DECLARE(MNT_RC_IN_PAN);
    MAVLINK_PARAM_DECLARE(MNT_RC_IN_TILT);
}mav_param_list_t;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void mavlink_param_send_all(void);
gp_err mavlink_param_set(const char* name, float val);
gp_err mavlink_param_send(const param_t* param);

// mavlink param (not used by FMT) api
void send_mavlink_param(char* name);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



