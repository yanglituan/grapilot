
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       mavproxy_monitor.h
  * @author     baiyang
  * @date       2021-7-22
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <common/gp_defines.h>

#include <c_library_v2/common/mavlink.h>
#include <c_library_v2/ardupilotmega/mavlink.h>

#include <rc_channel/rc_channel.h>
/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void mavproxy_rx_loop();

void mavproxy_rc_manual_override(RC_HandleTypeDef *c, int16_t value_in, const uint16_t offset, const float scaler, const uint32_t tnow, const bool reversed);

// allow override of RC channel values for complete GCS
// control of switch position and RC PWM values.
void mavproxy_handle_rc_channels_override(const mavlink_message_t *msg);

MAV_RESULT mavproxy_handle_command_component_arm_disarm(const mavlink_command_long_t *packet);

void mavproxy_proc_command(mavlink_command_long_t* command, mavlink_message_t* msg);

// return MAV_TYPE corresponding to frame class
MAV_TYPE mavproxy_get_frame_mav_type();
MAV_MODE mavproxy_base_mode();
MAV_STATE mavproxy_vehicle_system_status();
uint32_t mavproxy_custom_mode();

void mavproxy_send_banner();

MAV_RESULT mavlink_handle_command_do_set_mode(const mavlink_command_long_t *packet);
MAV_RESULT mavproxy_set_mode_common(const MAV_MODE _base_mode, const uint32_t _custom_mode);

void mavproxy_handle_set_mode(mavlink_message_t *msg);
/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



