
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       Dynamical.h
  * @author     baiyang
  * @date       2021-7-14
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/

/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
//螺旋桨
typedef struct
{
    float _diameter;        //螺旋桨直径
    float _pitch;           //螺距
    float _Ct;              //拉力系数(无量纲)
    float _Cm;              //扭矩系数
    float _mass;            //桨叶质量
    float _inertia;         //转动惯量
}Prop;

//电机（后续可支持电机的倾转）
typedef struct
{
    //Prop tProp;                //螺旋桨参数
    float _kv;                //KV值(rpm/volt)
    float _no_load_current;    //
    float _resistance;        //内阻 欧姆
    float _esc_resistance;  //电调内阻    欧姆
    float _max_current;        //最大电流

    float rpm;                //电机转速r/min
    float thrust;            //拉力N
    float torque;            //扭矩N.m    
    float current;            //电流

    float mot_pwm_min;
    float mot_pwm_max;
    float mot_spin_min;
    float mot_spin_max;
}Motor;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void Dynamical_Init(Motor* pMotor);
void Motor_SetParam(Motor* pMotor, float kv, float _resistance, float _esc_resistance, float no_load_current,
                    float mot_pwm_min, float mot_pwm_max, float mot_spin_min, float mot_spin_max);
void Prop_SetParam(Prop* pProp, float _diameter_in, float _pitch_in, float Ct, float Cm, float mass);
void Dynamical_Calculate_FM(Motor* pMotor, Prop* pProp, float voltage, float density, float dt);
float Motor_GetInCurrent(Motor* pMotor);
float Motor_PwmToCommand(Motor* pMotor, float pwm);

float Motor_GetFoce(Motor* pMotor);
float Motor_GetTorque(Motor* pMotor);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif

