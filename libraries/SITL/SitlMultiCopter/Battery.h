
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       Battery.h
  * @author     baiyang
  * @date       2021-7-14
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdint.h>
#include <common/filter/lpfilter.h>

/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    //参数
    float _capacity_ah;    //容量
    float _resistance;     //内阻
    float _max_voltage;    //最大电压

    //变量
    float voltage_set;
    float rem_ah;          //剩余电量
    //uint64_t last_us;

    LowPassFilt volta_filt;//输出电压的滤波器10HZ
}Battery;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void Battery_Setup(Battery* battery, float _capacity_Ah, float _resistance, float _max_voltage);
void Battery_InitVoltage(Battery* battery, float voltage);
void Batterty_SetCurrent(Battery* battery, float current, float dt);
float Batterty_GetVoltage(Battery* battery);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif

