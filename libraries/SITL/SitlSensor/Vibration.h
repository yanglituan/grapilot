
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       Vibration.h
  * @author     baiyang
  * @date       2021-7-14
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <common/gp_math/gp_mathlib.h>
#include <stdlib.h>

/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    bool enable_vib;
    float _weight;
    float _rpm_max;
    float _gain;

    //电机个数
    uint8_t num;
    float* vel_integ;    //积分器

}VibrationMode;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void VibrationMode_Init(VibrationMode* pVib, float weight, float rpm_max, float gain, float num);
void VibrationMode_Update(VibrationMode* pVib, Vector3f_t* pState_out, const Vector3f_t* pState_in, float* prop_vel, float dt);
void VibrationMode_Enable(VibrationMode* pVib);
void VibrationMode_Disable(VibrationMode* pVib);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif


