
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       ErrorMode.h
  * @author     baiyang
  * @date       2021-7-14
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <common/gp_math/gp_mathlib.h>
#include <common/filter/lpfilter.h>
#include <stdlib.h>

/*-----------------------------------macro------------------------------------*/

/*----------------------------------typedef-----------------------------------*/
typedef struct
{
    LowPassFilt_vec3f filter;
    Vector3f_t _scale;       //偏差量
    Vector3f_t _base;
    Vector3f_t _variance;    //高斯白噪声方差
    bool enable_err;
}ErrorMode;

/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void SensorMode_Init(ErrorMode* pSensor, Vector3f_t State, Vector3f_t* scale, Vector3f_t* base, Vector3f_t* variance);
void SensorMode_Update(ErrorMode* pSensor, Vector3f_t* pState_out, const Vector3f_t* pState_in, float dt);
void SensorMode_Enable(ErrorMode* pSensor);
void SensorMode_Disable(ErrorMode* pSensor);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif

