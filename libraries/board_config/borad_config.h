
/**
  ******************************************************************************
  * Copyright 2021 The grapilot Authors. All Rights Reserved.
  * 
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  * 
  * http://www.apache.org/licenses/LICENSE-2.0
  * 
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  * 
  * @file       borad_config.h
  * @author     baiyang
  * @date       2021-10-4
  ******************************************************************************
  */

#pragma once

#ifdef __cplusplus
extern "C"{
#endif

/*----------------------------------include-----------------------------------*/
#include <stdbool.h>
#include <rtconfig.h>
#include <common/gp_defines.h>
/*-----------------------------------macro------------------------------------*/
#ifndef GP_FEATURE_BOARD_DETECT
#if defined(HAL_RTTHREAD_ARCH_FMUV2) || defined(HAL_RTTHREAD_ARCH_FMUV3) || defined(HAL_RTTHREAD_ARCH_FMUV4) || defined(HAL_RTTHREAD_ARCH_FMUV5) || defined(HAL_RTTHREAD_ARCH_FMUV6)
#define GP_FEATURE_BOARD_DETECT 1
#else
#define GP_FEATURE_BOARD_DETECT 0
#endif
#endif
/*----------------------------------typedef-----------------------------------*/
// valid types for BRD_TYPE: these values need to be in sync with the
// values from the param description
typedef enum {
    BOARD_TYPE_UNKNOWN = -1,
    PX4_BOARD_AUTO     = 0,
    PX4_BOARD_PX4V1    = 1,
    PX4_BOARD_PIXHAWK  = 2,
    PX4_BOARD_PIXHAWK2 = 3,
    PX4_BOARD_PIXRACER = 4,
    PX4_BOARD_PHMINI   = 5,
    PX4_BOARD_PH2SLIM  = 6,
    PX4_BOARD_AEROFC   = 13,
    PX4_BOARD_PIXHAWK_PRO = 14,
    PX4_BOARD_AUAV21   = 20,
    PX4_BOARD_PCNC1    = 21,
    PX4_BOARD_MINDPXV2 = 22,
    PX4_BOARD_SP01     = 23,
    PX4_BOARD_FMUV5    = 24,
    VRX_BOARD_BRAIN51  = 30,
    VRX_BOARD_BRAIN52  = 32,
    VRX_BOARD_BRAIN52E = 33,
    VRX_BOARD_UBRAIN51 = 34,
    VRX_BOARD_UBRAIN52 = 35,
    VRX_BOARD_CORE10   = 36,
    VRX_BOARD_BRAIN54  = 38,
    PX4_BOARD_FMUV6    = 39,
    PX4_BOARD_OLDDRIVERS = 100,
} px4_board_type;

typedef struct {
    Param_int8 board_type;
    Param_int8 io_enable;
} board_state;

typedef struct {
    board_state state;
    bool _in_error_loop;
    px4_board_type px4_configured_board;
} board_config;
/*----------------------------------variable----------------------------------*/

/*-------------------------------------os-------------------------------------*/

/*----------------------------------function----------------------------------*/
void brd_init();
px4_board_type brd_get_board_type();

void brd_board_setup(board_config *brd);

void brd_config_error(const char *fmt, ...);

/*------------------------------------test------------------------------------*/

#ifdef __cplusplus
}
#endif



