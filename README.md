<p align="center">
  <a href="/LICENSE"><img src="https://img.shields.io/badge/license-Apache--2.0-brightgreen" alt="GitHub license" /></a>
</p>

## 概述
grapilot是使用C语言、基于面向对象编程思想实现的开源自驾仪。grapilot项目包括飞控软件和数字飞机，windows下的软件在环仿真使用十分简单。

核心算法借鉴px4、ardupilot、fmt的实现和思路，RTOS使用rtthread。该项目的初衷是降低学习飞控及了解px4、ardupilot的门槛，让更多的人可以快速了解飞控的使用、调试及原理。

**项目地址**：https://gitee.com/breederbai/grapilot
## 特点

grapilot具有轻量级，易于阅读和使用的特点，并且兼具稳定性和实时性。

- 支持多种编译工具，keil、vs2019工程由env工具自动生成。
- C 语言编写的轻量级飞控系统，更易使用和二次开发。
- 导航、控制算法借鉴于px4和ardupilot，成熟稳定。
- 支持当前最流行的开源飞控硬件 Pixhawk。
- 高实时性，时间误差 < 1us。
- 更高运行效率和更低的CPU使用率。提供更大算力空间用以提高算法复杂度和运行频率。
- 支持 Mavlink V2.0和主流地面站 QGC，Mission Planner等。
- 支持windows下的软件在环仿真。
- 高度模块化，松耦合的软件架构，易于裁剪和移植。

## 硬件平台

- **Pixhawk**: version 2.4.6 or 2.4.8.
-  未来将支持更多硬件. 

## 开发环境
grapilot支持多种工具进行开发：

- keil uvision5
- 工具链： RT-Thread env: [download](https://www.rt-thread.org/page/download.html)
- visual studio 2019 (用于Windows下的SITL)

## 编译

针对不同硬件平台的编译工程统一在grapilot/platform目录下

- Pixhawk硬件：
  - 在grapilot/platform/board/fmuv2目录下，用keil5打开project.uvprojx进行编译
  - 在grapilot/platform/board/fmuv2目录下，打开env工具（[教程](https://www.rt-thread.org/document/site/#/development-tools/env/env)），输入scons -j4进行编译
- windows下的软件在环仿真：
  - 在grapilot/platform/simulator/windows目录下，用vs2019打开project.sln，点击运行，即可启动软件仿真。
  - 使用QGC添加UDP连接方式，监听端口27750，目标主机127.0.0.1：27650。点击连接即可连接软件仿真的飞控。

## 微信公众号
grapilot

## 注意事项

1. grapilot使用utf8编码格式，国内默认编码格式为GB2312，在编译时有可能因为编码格式不同而导致报错（[I49FU8](https://gitee.com/breederbai/grapilot/issues/I49FU8)）。

## License

[Apache-2.0](./LICENSE)